<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1396149659169" ID="ID_1129079244" MODIFIED="1400020876867" TEXT="Definiciones">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      es la m&#225;s peque&#241;a desplegable y usable unidad de recursos web.
    </p>
  </body>
</html></richcontent>
<node CREATED="1396149707238" HGAP="50" ID="ID_137982845" MODIFIED="1397589477851" POSITION="right" TEXT="JPA" VSHIFT="6">
<node CREATED="1397587926444" ID="ID_1326897719" MODIFIED="1397595446431" TEXT="Conceptos">
<node CREATED="1396149749256" ID="ID_1905127148" MODIFIED="1397587968684" TEXT="Entity">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Termino introducido por Peter Chen[1] como elementos que tienen atributos y relaciones. Se espera los atributos y relaciones entre &#233;stos puedan ser almacenados en una base de datos relacional.
    </p>
    <p>
      
    </p>
    <p>
      [1]Peter P. Chen, &#8220;The entity-relationship model&#8212;toward a unified view of data,&#8221; ACM Transactions on Database Systems 1, no. 1 (1976): 9&#8211;36.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396149777025" ID="ID_1607236367" MODIFIED="1397587968711" TEXT="Persistencia">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es el concepto por el que se denomina a la capacidad que tiene un elemento, en particular una entidad, para poder ser almacenada en un medio y ser accedida a este en un tiempo posterior. Estricatmente hablando, un objeto persistente se convierte en &quot;persistente&quot;, valga&#160;&#160;la redudancia, una vez que es instanciado en memoria.[2] Es importante generar la distinci&#243;n anterior, pues una aplicaci&#243;n puede tener la ventaja de manipular una entidad sin que &#233;sta est&#233; necesariamente en estado &quot;persistente&quot;.
    </p>
    <p>
      
    </p>
    <p>
      [2] Pro JPA 2: Mastering the Java Persistence API, Mike Keith and Merrick Schincariol, p&#225;g. 18.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396149810355" ID="ID_1666162909" MODIFIED="1397587968731" TEXT="Identicity">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se denomina identificaci&#243;n de persistencia (persisten identity) o identificador (identifier) al identificador de una entidad que identifica una instancia de &#233;sta con su representaci&#243;n en el medio persistente y de otros entidades del mismo tipo.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396149841611" ID="ID_1700886758" MODIFIED="1397587968751" TEXT="Transaccionabilidad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Las entidades podr&#237;an ser denominadas &quot;quasi-transaccionales&quot;. Aunque ellas pueden se creadas, actualizadas y eliminadas, est&#225;s operaciones normalmente son realizadas en memoria, un contexto distindo al del medio persistente. Para que estos cambios se vean reflejados de deben &quot;commitear&quot; los cambios.&#160;&#160;&#160;
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396149868602" ID="ID_549212935" MODIFIED="1397587956477" TEXT="Granularidad">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Las entidades son estructuras que contienen diversos tipos de datos, y aunque pueden albergar una gran cantidad de datos intentan tener cantidades de datos bien acotadas a lo que representan.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396149899482" ID="ID_1241659221" MODIFIED="1397587956509" TEXT="Entity METADATA">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se a&#241;aden campos adicionales que contienen informaci&#243;n que no es almacenada en el medio persistente pero que son necesarias para realizar el proceso de transacci&#243;n. Esta informaci&#243;n puede ser incluida como &quot;Annotations&quot; o &quot;XML&quot;.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1397588020288" ID="ID_1975514842" MODIFIED="1397589476752" TEXT="ORM Object Relational Model">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Capacidad de mapear objetos en la base de datos y vice-versa.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1397595446432" ID="ID_680873807" MODIFIED="1397595637601" TEXT="contexto de persistencia">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es el grupo de entidades que son manejadas por un mismo EntityManager.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1397595638743" ID="ID_710506107" MODIFIED="1397596321544" TEXT="unidad de presistencia">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es un grupo l&#243;gico de clases que con persistables con opciones relacionadas.
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1397587943959" ID="ID_70175480" MODIFIED="1397587953825" TEXT="Programaci&#xf3;n"/>
<node CREATED="1397589479497" ID="ID_656329667" MODIFIED="1397589755813" TEXT="@notations">
<node CREATED="1397589501009" ID="ID_1245730732" MODIFIED="1397589570353" TEXT="@Table">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      @Table(name=&quot;NEWTABLENAME&quot;)&#160;&#160;con esto se define el nombre de la tabla
    </p>
    <p>
      
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1397589559685" ID="ID_1580638109" MODIFIED="1397589610133" TEXT="@Id">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Se agrega al atributo que servira de clave primaria.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1397589611227" ID="ID_260720625" MODIFIED="1397589730480" TEXT="@GeneratedValue">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Junto con ID define si este valor ser&#225; generado autom&#225;ticamente.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1397589622634" ID="ID_128529963" MODIFIED="1397589699009" TEXT="@Transient">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      se le agrega a los campos que no almacenar&#225;n en la base de datos
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1397589757935" ID="ID_638724073" MODIFIED="1397595438008" TEXT="@Column">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      @Column(&quot;newColumnName&quot;) define el nombre de la columna a la que corresponde el campo.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
<node CREATED="1396149696450" HGAP="46" ID="ID_1558635231" MODIFIED="1400020880019" POSITION="left" TEXT="J2EE" VSHIFT="10">
<node CREATED="1396149716090" ID="ID_980629627" MODIFIED="1396150013667" TEXT="Servlets">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Clases Java que que procesa dinamicamente solicitudes (request) y construye respuestas (Responses).
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396150016611" ID="ID_1184091642" MODIFIED="1396152234203" TEXT="JavaServer Faces">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      La tecnolog&#237;a utilizada por J2EE para generar presentaci&#243;n web.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396150026784" ID="ID_879696269" MODIFIED="1396152305142" TEXT="Facelets">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Es el lenguaje en el cual se desarrollan las presentaciones generadas por la tecnolog&#237;a JavaServer Faces. Este lenguaje&#160;&#160;es el que reemplaza a JSP.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396151395616" ID="ID_1000826572" MODIFIED="1396211007030" TEXT="web module">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      es la m&#225;s peque&#241;a desplegable y usable unidad de recursos web.
    </p>
    <p>
      
    </p>
    <p>
      Puede considerarse una <b>web module</b>&#160;una forma general de denominar a los <b>web container</b>&#160;o a los <b>web components</b>
    </p>
  </body>
</html></richcontent>
<node CREATED="1396150119404" ID="ID_1294923088" MODIFIED="1396211027497" TEXT="web container (servicios)">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Un contenedor web proporciona servicios como la solicitud de despacho, seguridad, concurrencia, y la gesti&#243;n del ciclo de vida. Un contenedor Web tambi&#233;n proporciona acceso a APIs de componentes web tales como nombres, transacciones, y correo electr&#243;nico.
    </p>
  </body>
</html></richcontent>
</node>
<node CREATED="1396150541592" ID="ID_615849997" MODIFIED="1396151389775" TEXT="Web Component">
<richcontent TYPE="NOTE"><html>
  <head>
    
  </head>
  <body>
    <p>
      son parte del entorno web como nombres de dominio, transacciones, y correo electr&#243;nico.
    </p>
    <p>
      
    </p>
    <p>
      Algunos componentes son estaticos, como por ejemplo,imagenes.
    </p>
  </body>
</html></richcontent>
</node>
</node>
</node>
</node>
</map>
