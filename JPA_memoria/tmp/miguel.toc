\select@language {spanish}
\contentsline {chapter}{\numberline {1}Introducción}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Tipos de persistencia en Java}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}JDBC}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}MyBatis}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Java Data Objects (JDO)}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}Java Persistent API (JPA)}{4}{section.2.4}
\contentsline {chapter}{\numberline {3}Conceptos básicos en JPA}{6}{chapter.3}
\contentsline {section}{\numberline {3.1}Descripción General}{6}{section.3.1}
\contentsline {section}{\numberline {3.2}Conceptos Básicos}{7}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Persistencia}{7}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Objeto Persistente}{7}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Contexto de Persistencia}{7}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Unidad de Persistencia}{7}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}Transacciones JTA}{8}{subsection.3.2.5}
\contentsline {subsection}{\numberline {3.2.6}Transacciones Locales}{8}{subsection.3.2.6}
\contentsline {section}{\numberline {3.3}Mapeo de datos}{9}{section.3.3}
\contentsline {section}{\numberline {3.4}Querys:}{10}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Query}{10}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}CRITERIA Query}{10}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}TypedQuery<T> e}{10}{subsection.3.4.3}
\contentsline {chapter}{\numberline {4}Proveedores de Persistencia JPA}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}DataNucleus}{11}{section.4.1}
\contentsline {section}{\numberline {4.2}Hibernate}{11}{section.4.2}
\contentsline {section}{\numberline {4.3}OpenJPA}{12}{section.4.3}
\contentsline {section}{\numberline {4.4}Oracle Toplink}{12}{section.4.4}
\contentsline {section}{\numberline {4.5}EclipseLink}{12}{section.4.5}
\contentsline {section}{\numberline {4.6}ObjectDB}{12}{section.4.6}
\contentsline {chapter}{\numberline {5}Estudios Comparativos}{14}{chapter.5}
\contentsline {section}{\numberline {5.1}Santiago Rodríguez}{14}{section.5.1}
\contentsline {section}{\numberline {5.2}ObjectDB Software Ltd.}{15}{section.5.2}
\contentsline {section}{\numberline {5.3}Lukás Sembera}{16}{section.5.3}
\contentsline {section}{\numberline {5.4}DZone}{16}{section.5.4}
\contentsline {chapter}{\numberline {6}Criterios y Pruebas}{18}{chapter.6}
\contentsline {section}{\numberline {6.1}Criterios}{18}{section.6.1}
\contentsline {section}{\numberline {6.2}Criterios}{18}{section.6.2}
\contentsline {section}{\numberline {6.3}Modelo representativo}{20}{section.6.3}
\contentsline {section}{\numberline {6.4}Descripción Pruebas comparativas}{21}{section.6.4}
\contentsline {section}{\numberline {6.5}Descripciones de hardware software}{21}{section.6.5}
\contentsline {section}{\numberline {6.6}Resultados}{22}{section.6.6}
\contentsline {chapter}{\numberline {7}Diseño de la solución de persistencia para el Sistema de Gestión de Eventos y Usuarios}{23}{chapter.7}
\contentsline {section}{\numberline {7.1}Elección de la implementación}{23}{section.7.1}
\contentsline {section}{\numberline {7.2}Esquema de datos}{23}{section.7.2}
\contentsline {section}{\numberline {7.3}Convesión de datos}{23}{section.7.3}
\contentsline {section}{\numberline {7.4}Funciones para el acceso de datos}{23}{section.7.4}
\contentsline {chapter}{\numberline {8}Evaluación del Sistema de Gestión de Eventos y Usuarios y Resultados}{25}{chapter.8}
\contentsline {section}{\numberline {8.1}Descripción Pruebas comparativas}{25}{section.8.1}
\contentsline {section}{\numberline {8.2}Descripción Pruebas usando librerías exclusivas del estándar}{25}{section.8.2}
\contentsline {chapter}{\numberline {9}Conclusiones}{26}{chapter.9}
\contentsline {section}{\numberline {9.1}Ejemplos de inserción de código}{26}{section.9.1}
